import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './assets/css/bootstrap.min.css';
import './assets/css/font-awesome/css/font-awesome.css'; 
import "./assets/css/bootstrap2-toggle.css";
import './index.css';
import App from './App';
import store from "./store"
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
