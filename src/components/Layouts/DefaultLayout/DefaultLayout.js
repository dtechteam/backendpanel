import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import  './DefaultLayout.css';
import Header from './Header';
import Footer from './Footer';
import routes from '../../../routes';
class DefaultLayout extends Component {
    render() {
        return (
            <React.Fragment>
                <header className="header"><Header/></header>
                <main className = "main" >
                    <Container fluid>
                        <Switch>
                            {routes.map((route, idx) => {
                                return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                                    <route.component {...props} />
                                )} />)
                                : (null);
                            },
                            )}
                            <Redirect from="/" to="/dashboard" />
                            </Switch>
                    </Container>
                </main>
                <footer className="sticky-footer"><Footer/></footer>
            </React.Fragment>
        );
    }
}

export default DefaultLayout;
