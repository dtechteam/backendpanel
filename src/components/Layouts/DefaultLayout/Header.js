import React, { Component } from 'react';
import { Nav, NavItem, NavLink, Navbar, NavbarBrand,  Collapse, NavbarToggler  } from 'reactstrap';
import { Link } from 'react-router-dom';

class Header extends Component{
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			isOpen: false
		};
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
    render() {
        return (
			<React.Fragment>
				<Navbar id = "mainNav" className = " navbar-expand-lg navbar-dark bg-dark fixed-top" >
				<NavbarBrand href = "/" > Order System </NavbarBrand>
				 <NavbarToggler onClick={this.toggle} />
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className = "navbar-sidenav" navbar >
							<NavItem >
								<Link to = "/dashboard" > Dashboard </Link>
							</NavItem>
							<NavItem >
								<Link to = "/profile" > Profile </Link>
							</NavItem>
							<NavItem >
								<Link to = "/category" > Category </Link>
							</NavItem>
							<NavItem >
								<Link to = "/attribute-set" > Attribute Set </Link>
							</NavItem>							
							<NavItem >
								<Link to = "/product" > Product </Link>
							</NavItem>
							<NavItem >
								<Link to = "/change-password" > Chamge password </Link>
							</NavItem>
						</Nav>

						<Nav className = "sidenav-toggler"	navbar >
							<NavItem >
								<NavLink href="/" className = "text-center" id="sidenavToggler">
									<i className="fa fa-fw fa-angle-left"></i>
								</NavLink>
							</NavItem>
						</Nav>
						<Nav className = "ml-auto"	navbar >
							<NavItem >
								<Link to="/login" className = "text-center" id="sidenavToggler">
								<i className = "fa fa-fw fa-sign-out" > </i>Logout
								</Link>
							</NavItem>
						</Nav>

					</Collapse>
				</Navbar>
			</React.Fragment>
        )
    }
}
export default Header;