import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import LteHeader from './Header';
import LteFooter from './Footer';
import MainSidebar from './MainSidebar';
import routes from '../../../routes';
import  './LteLayout.css';
class LteLayout extends Component {
    render() {
        return (
            <React.Fragment>
                <header class="main-header"><LteHeader/></header>
                <aside className="main-sidebar"><MainSidebar/></aside>
                <div class="content-wrapper">
                    <Container fluid>
                        <Switch>
                            {routes.map((route, idx) => {
                                return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                                    <route.component {...props} />
                                )} />)
                                : (null);
                            },
                            )}
                            <Redirect from="/" to="/dashboard" />
                            </Switch>
                    </Container>
                </div>
                <footer class="main-footer"><LteFooter/></footer>
            </React.Fragment>
        );
    }
}

export default LteLayout;
