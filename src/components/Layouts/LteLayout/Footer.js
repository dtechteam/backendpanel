import React, { Component } from 'react';

class LteFooter extends Component{
    render() {
        return (
			
				<React.Fragment>
				    <div className="pull-right hidden-xs">
				      <b>Version</b> 2.4.0
				    </div>
				    <strong>Copyright &copy; 2018.</strong> All rights reserved.
				</React.Fragment>
				
        )
    }
}
export default LteFooter;