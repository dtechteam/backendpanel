import React, { Component } from 'react';
import { Nav, NavItem, NavLink, Navbar, NavbarBrand,  Collapse, NavbarToggler  } from 'reactstrap';
import { Link } from 'react-router-dom';

class MainSidebar extends Component{
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			isOpen: false
		};
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
    render() {
        return (
			<React.Fragment>
			
				    { /*  sidebar: style can be found in sidebar.less  */}
				    <section className="sidebar">
				      { /*  Sidebar user panel  */}
				      <div className="user-panel">
				        <div className="pull-left image">
				          <img src="./assets/images/user2-160x160.jpg" className="img-circle" alt="User Image"/>
				        </div>
				        <div className="pull-left info">
				          <p>Ravinder</p>
				          <a href="#"><i className="fa fa-circle text-success"></i> Online</a>
				        </div>
				      </div>
				      { /*  search form  */}
				      <form action="#" method="get" className="sidebar-form">
				        <div className="input-group">
				          <input type="text" name="q" className="form-control" placeholder="Search..."/>
				              <span className="input-group-btn">
				                <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
				                </button>
				              </span>
				        </div>
				      </form>
				      { /*  /.search form  */ }
				      { /*  sidebar menu: : style can be found in sidebar.less  */ }
				      <ul className="sidebar-menu" data-widget="tree">
				        <li className="header">MAIN NAVIGATION</li>
				        <li className="treeview">
				          <a href="#">
				            <i className="fa fa-dashboard"></i> <span>Dashboard</span>
				            <span className="pull-right-container">
				              <i className="fa fa-angle-left pull-right"></i>
				            </span>
				          </a>
				          <ul className="treeview-menu">
				            <li><a href="../../index.html"><i className="fa fa-circle-o"></i> Dashboard v1</a></li>
				            <li><a href="../../index2.html"><i className="fa fa-circle-o"></i> Dashboard v2</a></li>
				          </ul>
				        </li>
				        
				        <li><a href="https://adminlte.io/docs"><i className="fa fa-book"></i> <span>Documentation</span></a></li>
				      </ul>
				    </section>
				    { /*  /.sidebar  */}
				  
			</React.Fragment>
        )
    }
}
export default MainSidebar;