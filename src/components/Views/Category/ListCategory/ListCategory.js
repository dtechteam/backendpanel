import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row} from 'reactstrap';import { Link } from 'react-router-dom';
// Import React Table
import ReactTable from "react-table";
import { connect } from 'react-redux';
import "react-table/react-table.css";
//import ListCategoryData from './ListCategoryData';
import { fetchCategories, deleteCategory } from '../../../../actions/category-actions';

class ListCategory extends Component{
	componentDidMount() {
		this.props.fetchCategories();
	}

	render() {
		const columns = [{
			Header: 'Id',
			accessor: 'id',
			show: false
		},
		{
		    Header: 'Category',
		    accessor: 'category',
			Cell: row => {
              return (
                <div>
                  <Link to={'category/edit/'+row.row.category.id}>{row.row.category.name}</Link>
                </div>
              )
            }
		},
		{
		    Header: 'Status',
		    accessor: 'status'
		}];

		const data = [];
		this.props.categoryList.map(category => {
			const categoryArr = {};
    		categoryArr.id = category._id;
    		categoryArr.category = {};
    		categoryArr.category.name = category.name;
    		categoryArr.category.id = category._id;
    		categoryArr.status = category.status;
    		data.push(categoryArr);
		});

		return (
			<React.Fragment>
				<Breadcrumb>
					<BreadcrumbItem active>Category</BreadcrumbItem>
					<div className="addTopFrmBtnWrapper"><Link to={'/category/add'} className="addTopFrmBtn">Add</Link></div>
				</Breadcrumb>
				<Row>
					<Col sm="12">
						<ReactTable
							data={data}
							columns={columns}

							defaultPageSize={5}
							className="-striped -highlight"
						/>
					</Col>
				</Row>
			</React.Fragment>
			
		);
		
	}
}

function mapCategoryState(state) {
	return {
		categoryList: state.categoryStore.categories,
		loading: state.categoryStore.loading,
		errors: state.categoryStore.errors
	}
}

export default connect(mapCategoryState, { fetchCategories, deleteCategory })(ListCategory);
