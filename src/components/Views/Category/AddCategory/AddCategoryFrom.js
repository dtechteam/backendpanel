import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row, Button, Form, FormGroup, Label } from 'reactstrap';
import { Field, reduxForm } from 'redux-form';
import classnames from 'classnames';

const validate = (values) => {
    const errors = {};
    
    if (!values.name) {
        errors.name = {
            message: 'You need to provide a Category Name'
        }
    }
    if (!values.description) {
        errors.description = {
            message: 'You need to provide a Category Description'
        }
    }
    return errors;
}

const warn = values => {
  const warnings = {}  
  return warnings
}

const renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning }
}) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} />
            {touched &&
            ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
        </div>
    </div>
)
class AddCategoryForm extends Component {
    componentWillReceiveProps = (nextProps) => { // Load Category Asynchronously
        const { category } = nextProps;
        if (category._id !== this.props.category._id) {
            this.props.initialize(category)
        }
    }

    render() {
        const { handleSubmit, pristine, submitting, loading, category, reset } = this.props;
        return (
            <React.Fragment>
                <Breadcrumb>
                    <BreadcrumbItem active>{category._id ? 'Edit Category' : 'Add New Category'}</BreadcrumbItem>
                </Breadcrumb>

                <Row>
                    <Col sm="12">
                        <Form onSubmit={handleSubmit} loading={loading}>
                            <FormGroup row>
                                <Col sm="6">
                                    <Field name="name" type="text" component={renderField} label="Category Name" />
                                </Col>

                                <Col sm="6">
                                    <Label for="files">Image</Label>
                                    <input className="fullwidth" type="file"  />
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Col sm="12">
                                    <Label for="description">Description</Label>
                                    <textarea className="form-control" name="description" id="description" placeholder="Description" />
                                </Col>
                            </FormGroup>

                            <Button className="px-4" type="submit" disabled={submitting}>Submit</Button>
                            <Button className="px-4" type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</Button>
                        </Form>
                    </Col>
                </Row>
            </React.Fragment>
        )
    }
}
export default reduxForm({ form: 'category', validate, warn })(AddCategoryForm);