import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { newCategory, saveCategory, fetchCategory, updateCategory } from '../../../../actions/category-actions';
import AddCategoryForm from './AddCategoryFrom';

class AddCategory extends Component {

  state = {
    redirect: false
  }

  componentDidMount = () => {
    const { _id } = this.props.match.params;
    if (_id) {
      this.props.fetchCategory(_id)
    } else {
      this.props.newCategory();
    }
  }

  submit = (category) => {
    if (!category._id) {
      return this.props.saveCategory(category)
        .then(response => this.setState({ redirect: true }))
        .catch(err => {
          throw new SubmissionError(this.props.errors)
        })
    } else {
      return this.props.updateCategory(category)
        .then(response => this.setState({ redirect: true }))
        .catch(err => {
          throw new SubmissionError(this.props.errors)
        })
    }
  }

  render() {
    return (
      <div>
        {
          this.state.redirect ?
            <Redirect to="/category" /> :
            <AddCategoryForm category={this.props.category} loading={this.props.loading} onSubmit={this.submit} />
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    category: state.categoryStore.category,
    errors: state.categoryStore.errors
  }
}

export default connect(mapStateToProps, { newCategory, saveCategory, fetchCategory, updateCategory })(AddCategory);
