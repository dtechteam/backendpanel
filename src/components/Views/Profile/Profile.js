import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row, Button, Form, FormGroup, Label, Input} from 'reactstrap';

class Profile extends Component{
	constructor(props) {
		super(props);

		this.state = {
           	fields: {},
           	errors: {}
		}
	}

	handleValidation(){
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        
        if(!fields["name"]){
           	formIsValid = false;
           	errors["name"] = "Cannot be empty";
        }
        if(typeof fields["name"] !== "undefined"){
           	if(!fields["name"].match(/^[a-zA-Z]+$/)){
              	formIsValid = false;
              	errors["name"] = "Only letters";
           	}        
        } 

        if(!fields["email"]){
           	formIsValid = false;
           	errors["email"] = "Cannot be empty";
        }
        if(typeof fields["email"] !== "undefined"){
           	let lastAtPos = fields["email"].lastIndexOf('@');
           	let lastDotPos = fields["email"].lastIndexOf('.');

           	if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
              	formIsValid = false;
              	errors["email"] = "Email is not valid";
            }
       	}

       	if(!fields["mobile"]){
       		formIsValid = false;
           	errors["mobile"] = "Cannot be empty";
        }
        if(typeof fields["mobile"] !== "undefined"){
           	if(!fields["mobile"].match(/^[0-9]+$/)){
              	formIsValid = false;
              	errors["mobile"] = "Only numbers";
           	}        
        }

       	this.setState({errors: errors});
       	return formIsValid;
   	}

	handleSubmit(event) {  		
		event.preventDefault();
  		if(this.handleValidation()){
           alert('A name was submitted: ' + this.state.value);
        }else{
           	//alert("Form has errors.")
        }
	}

	handleChange(field, e){         
    	let fields = this.state.fields;
        fields[field] = e.target.value;        
        this.setState({fields});
    }

    render() {
        return (
		    <React.Fragment>
		      <Breadcrumb>
				<BreadcrumbItem active>Profile</BreadcrumbItem>
			</Breadcrumb>

		      	<Row>
		      		<Col sm="12">
		      			 <Form onSubmit={this.handleSubmit.bind(this)}>
							<FormGroup row>
								<Col sm="6">
									<Label for="name">Name</Label>
									<Input ref="name" type="text" id="name" placeholder="Enter Name" onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]} />
									<span style={{color: "red"}}>{this.state.errors["name"]}</span>
								</Col>
								<Col sm="6">
									<Label for="email">Email</Label>
									<Input ref="email" type="text" id="email" placeholder="Enter Email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} />
									<span style={{color: "red"}}>{this.state.errors["email"]}</span>
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="6">
									<Label for="mobile">Mobile Number</Label>
									<Input ref="mobile" type="text" id="mobile" placeholder="Enter Mobile Number" onChange={this.handleChange.bind(this, "mobile")} value={this.state.fields["mobile"]} />
									<span style={{color: "red"}}>{this.state.errors["mobile"]}</span>
								</Col>
							</FormGroup>

							<Button className="px-4" type="submit">Submit</Button>
						</Form>

		      		</Col>
		      	</Row>
			</React.Fragment >
        )
    }
}
export default Profile;