import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row, Button, Form, FormGroup, Label, Input} from 'reactstrap';
import { Link } from 'react-router-dom';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

class AttributeOptionAdd extends Component{
	constructor(props) {
		super(props);

		this.state = {
       		fields: {},
           	errors: {}
		}
	}

	handleValidation(){
    	let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        
        if(!fields["attrname"]){
       		formIsValid = false;
           	errors["attrname"] = "Cannot be empty";
        }

        if(typeof fields["attrname"] !== "undefined"){
           	if(!fields["attrname"].match(/^[a-zA-Z]+$/)){
          		formIsValid = false;
              	errors["attrname"] = "Only letters";
           	}
        }

        if(!fields["attrvalue"]){
       		formIsValid = false;
           	errors["attrvalue"] = "Cannot be empty";
        }

        if(typeof fields["attrvalue"] !== "undefined"){
       		if(!fields["attrvalue"].match(/^[a-zA-Z]+$/)){
          		formIsValid = false;
              	errors["attrvalue"] = "Only letters";
           	}
        }

       	this.setState({errors: errors});
       	return formIsValid;
   	}

	handleSubmit(event) {
		event.preventDefault();

        if(this.handleValidation()){
           	alert('A name was submitted: ' + this.state.value);
        }else{
           	//alert("Form has errors.")
        }
	}

	handleChange(field, e){         
    	let fields = this.state.fields;
        fields[field] = e.target.value;        
        this.setState({fields});
    }

    render() {
    	const data = [
    		{
	    		id: 1,
			    attrset: {
      				name: 'Attr 1',
      				id: 1
  				},
			    status: 'Active',
			    action: {
			    	id: 1
			    }
			},
			{
	    		id: 2,
			    attrset: {
      				name: 'Attr 2',
      				id: 2
  				},
			    status: 'Active',
			    action: {
			    	id: 2
			    }
			},
			{
	    		id: 3,
			    attrset: {
      				name: 'Attr 3',
      				id: 3
  				},
			    status: 'Active',
			    action: {
			    	id: 3
			    }
			},
			{
	    		id: 4,
			    attrset: {
      				name: 'Attr 4',
      				id: 4
  				},
			    status: 'Active',
			    action: {
			    	id: 4
			    }
			}
		]

		const columns = [{
		    Header: 'Id',
		    accessor: 'id',
		    show: false
		},
		{
		    Header: 'Attribute Set',
		    accessor: 'attrset',
			Cell: row => {
              	return (
                	<div>
                  		<Link to={'attrset/add/'+row.row.attrset.id}>{row.row.attrset.name}</Link>
            		</div>
              	)
            }
		},
		{
		    Header: 'Status',
		    accessor: 'status'
		},
		{
		    Header: 'Action',
		    accessor: 'action',
		    Cell: row => {
		    	return (
                	<div>
                		<Link to={'attribute-option/'+row.row.action.id}>{'Add'}</Link>
                	</div>
              	)
            }
		}]

        return (
		    <React.Fragment>
		      	<Breadcrumb>
					<BreadcrumbItem active>Attribute Option</BreadcrumbItem>
				</Breadcrumb>

		      	<Row>
		      		<Col sm="12">
		      			 <Form onSubmit={this.handleSubmit.bind(this)}>
							<FormGroup row>
								<Col sm="6">
									<Label for="attrlabel">Label</Label>
									<Input ref="attrname" type="text" id="attrlabel" placeholder="Label" onChange={this.handleChange.bind(this, "attrname")} value={this.state.fields["attrname"]} />
									<span style={{color: "red"}}>{this.state.errors["attrname"]}</span>
								</Col>
								
								<Col sm="6">
									<Label for="attrvalue">Value</Label>
									<Input refs="attrvalue" type="text" id="attrvalue" placeholder="Value" onChange={this.handleChange.bind(this, "attrvalue")} value={this.state.fields["attrvalue"]} />
									<span style={{color: "red"}}>{this.state.errors["attrvalue"]}</span>
								</Col>
							</FormGroup>

							<Button className="px-4" type="submit">Submit</Button>
						</Form>
		      		</Col>
		      	</Row>

		      	<Row className="attr-option-list">
		      		<Col sm="12">
						<ReactTable
							data={data}
							columns={columns}

							defaultPageSize={5}
							className="-striped -highlight"
						/>
		      		</Col>
		      	</Row>
			</React.Fragment >
        )
    }
}
export default AttributeOptionAdd;