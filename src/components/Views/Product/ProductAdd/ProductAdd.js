import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row, Button, Form, FormGroup, Label, Input} from 'reactstrap';
import Switch from "react-switch";

class ProductAdd extends Component{
	constructor(props) {
		super(props);
		this.state = { checked: false };
    	this.handleChange = this.handleChange.bind(this);
    	this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(checked) {
    	this.setState({ checked });
  	}

	handleSubmit(event) {
  		alert('A name was submitted: ' + this.state.value);
  		event.preventDefault();
	}

    render() {
        return (
		    <React.Fragment>
		      <Breadcrumb>
				<BreadcrumbItem active>Product Add</BreadcrumbItem>
			</Breadcrumb>

		      	<Row>
		      		<Col sm="12">
		      			 <Form onSubmit={this.handleSubmit}>
							<FormGroup row>
								<Col sm="6">
									<Label for="name">Product Name</Label>
									<Input type="text" name="name" id="name" placeholder="Product Name" />
								</Col>

								<Col sm="6">
									<Label for="catId"> Select Ctegory</Label>
									<Input type="select" name="catid" id="catId">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</Input>
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="6">
									<Label for="price">Price</Label>
									<Input type="text" name="price" id="price" placeholder="Price" />
								</Col>

								<Col sm="6">
									<Label for="saleprice">Sale Price</Label>
									<Input type="text" name="saleprice" id="saleprice" placeholder="Sale Price" />
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="6">
									<Label for="title">Image</Label>
									<input class="fullwidth" type="file" multiple onChange={ (e) => this.handleChange(e.target.files) } />
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="12">
									<Label for="description">Description</Label>
									<textarea class="form-control" name="description" id="description" placeholder="Description" />
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="12">
									<Label for="title">Status</Label>
									<div class="fullwidth">
										<Switch
		          							onChange={this.handleChange}
		          							checked={this.state.checked}
		          							id="normal-switch"
		        						/>
		        					</div>
	        					</Col>
							</FormGroup>
							
							<Button className="px-4" type="submit">Submit</Button>
						</Form>
		      		</Col>
		      	</Row>
			</React.Fragment >
        )
    }
}
export default ProductAdd;