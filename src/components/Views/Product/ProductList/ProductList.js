import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row} from 'reactstrap';
import { Link } from 'react-router-dom';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

class ProductList extends Component{

	
    render() {
    	const data = [
    		{
	    		id: 1,
			    product: {
      				name: 'blue shirt 1',
      				id: 1
  				},
			    status: 'Active'
			},
			{
	    		id: 2,
			    product: {
      				name: 'blue shirt 2',
      				id: 2
  				},
			    status: 'Active'
			},
			{
	    		id: 3,
			    product: {
      				name: 'blue shirt 3',
      				id: 3
  				},
			    status: 'Active'
			},
			{
	    		id: 4,
			    product: {
      				name: 'blue shirt 4',
      				id: 4
  				},
			    status: 'Active'
			},
			{
	    		id: 5,
			    product: {
      				name: 'blue shirt 5',
      				id: 5
  				},
			    status: 'Active'
			},
			{
	    		id: 6,
			    product: {
      				name: 'blue shirt 6',
      				id: 6
  				},
			    status: 'Active'
			},
			{
	    		id: 7,
			    product: {
      				name: 'blue shirt 7',
      				id: 7
  				},
			    status: 'Active'
			},
			{
	    		id: 8,
			    product: {
      				name: 'blue shirt 8',
      				id: 8
  				},
			    status: 'Active'
			},
			{
	    		id: 9,
			    product: {
      				name: 'blue shirt 9',
      				id: 9
  				},
			    status: 'Active'
			}
		]

		const columns = [{
		    Header: 'Id',
		    accessor: 'id',
		    show: false
		},
		{
		    Header: 'Product',
		    accessor: 'product',
			Cell: row => {
              return (
                <div>
                  <Link to={'product/add/'+row.row.product.id}>{row.row.product.name}</Link>
                </div>
              )
            }
		},
		{
		    Header: 'Status',
		    accessor: 'status'
		}]

        return (
		    <React.Fragment>
		      	<Breadcrumb>
					<BreadcrumbItem active>Product</BreadcrumbItem>
					<div className="addTopFrmBtnWrapper"><Link to={'/product/add'} className="addTopFrmBtn">Add</Link></div>
				</Breadcrumb>
		      	<Row>
		      		<Col sm="12">
						<ReactTable
							data={data}
							columns={columns}

							defaultPageSize={5}
							className="-striped -highlight"
						/>
		      		</Col>
		      	</Row>
			</React.Fragment >
        )
    }
}
export default ProductList;