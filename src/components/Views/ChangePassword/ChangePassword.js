import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row, Button, Form, FormGroup, Label, Input} from 'reactstrap';

class ChangePassword extends Component{
	constructor(props) {
		super(props);
		this.state = { value: '' };

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(event) {
  		alert('A name was submitted: ' + this.state.value);
  		event.preventDefault();
	}

    render() {
        return (
		    <React.Fragment>
		      <Breadcrumb>
				<BreadcrumbItem active>Change Password</BreadcrumbItem>
			</Breadcrumb>

		      	<Row>
		      		<Col sm="12">
		      			 <Form onSubmit={this.handleSubmit}>
							<FormGroup row>
								<Col sm="6">
									<Label for="oldpasswor">Old password</Label>
									<Input type="password" name="oldpasswor" id="oldpasswor" placeholder="Old password" />
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="6">
									<Label for="newpassword">New password</Label>
									<Input type="password" name="newpassword" id="newpassword" placeholder="New password" />
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="6">
									<Label for="conpasswor">Confirm password</Label>
									<Input type="password" name="conpasswor" id="conpasswor" placeholder="Confirm password" />
								</Col>
							</FormGroup>							
							
							<Button className="px-4" type="submit">Submit</Button>
						</Form>
		      		</Col>
		      	</Row>
			</React.Fragment >
        )
    }
}
export default ChangePassword;