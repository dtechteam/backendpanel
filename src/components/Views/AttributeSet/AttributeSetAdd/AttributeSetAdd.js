import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row, Button, Form, FormGroup, Label, Input} from 'reactstrap';
import Switch from "react-switch";

class AttributeSetAdd extends Component{
	constructor(props) {
		super(props);
		this.state = { checked: false };
    	
    	this.state = {
       		fields: {},
           	errors: {}
		}
	}

	handleChangeToggle(checked) {
    	this.setState({ checked });
  	}

  	handleValidation(){
    	let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        
        if(!fields["attrtitle"]){
       		formIsValid = false;
           	errors["attrtitle"] = "Cannot be empty";
        }

        if(typeof fields["attrtitle"] !== "undefined"){
           	if(!fields["attrtitle"].match(/^[a-zA-Z]+$/)){
          		formIsValid = false;
              	errors["attrtitle"] = "Only letters";
           	}
        }

        if(!fields["attrtype"]){
       		formIsValid = false;
           	errors["attrtype"] = "Cannot be empty";
        }

        if(typeof fields["attrtype"] !== "undefined"){
           	if(!fields["attrtype"].match(/^[a-zA-Z0-9]+$/)){
          		formIsValid = false;
              	errors["attrtype"] = "Only letters";
           	}
        }        

       	this.setState({errors: errors});
       	return formIsValid;
   	}

	handleSubmit(event) {
  		event.preventDefault();

        if(this.handleValidation()){
           	alert('A name was submitted: ' + this.state.value);
        }else{
           	//alert("Form has errors.")
        }
	}

	handleChange(field, e){         
    	let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    }

    render() {
        return (
		    <React.Fragment>
		      	<Breadcrumb>
					<BreadcrumbItem active>Add Attribute Set</BreadcrumbItem>
				</Breadcrumb>

		      	<Row>
		      		<Col sm="12">
		      			 <Form onSubmit={this.handleSubmit.bind(this)}>
							<FormGroup row>
								<Col sm="6">
									<Label for="attrtitle">Attribute Name</Label>
									<Input ref="attrtitle" type="text" id="attrtitle" placeholder="Attribute Name" onChange={this.handleChange.bind(this, "attrtitle")} value={this.state.fields["attrtitle"]} />
									<span style={{color: "red"}}>{this.state.errors["attrtitle"]}</span>
								</Col>

								<Col sm="6">
									<Label for="attrtype">Attribute Type</Label>
									<Input ref="attrtype" type="text" id="attrtype" placeholder="Attribute Type" onChange={this.handleChange.bind(this, "attrtype")} value={this.state.fields["attrtype"]} />
									<span style={{color: "red"}}>{this.state.errors["attrtype"]}</span>
								</Col>
							</FormGroup>

							<FormGroup row>
								<Col sm="12">
									<Label for="title">Status</Label>
									<div class="fullwidth">
										<Switch
		          							onChange={this.handleChangeToggle.bind(this)}
		          							checked={this.state.checked}
		          							id="normal-switch"
		        						/>
		        					</div>
	        					</Col>
							</FormGroup>						
							
							<Button className="px-4" type="submit">Submit</Button>
						</Form>
		      		</Col>
		      	</Row>
			</React.Fragment >
        )
    }
}
export default AttributeSetAdd;