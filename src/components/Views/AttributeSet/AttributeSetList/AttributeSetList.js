import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Col, Row} from 'reactstrap';
import { Link } from 'react-router-dom';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

class AttributeSetList extends Component{
	

    render() {
    	const data = [
    		{
	    		id: 1,
			    attrset: {
      				name: 'Attr 1',
      				id: 1
  				},
			    status: 'Active',
			    action: {
			    	id: 1
			    }
			},
			{
	    		id: 2,
			    attrset: {
      				name: 'Attr 2',
      				id: 2
  				},
			    status: 'Active',
			    action: {
			    	id: 2
			    }
			},
			{
	    		id: 3,
			    attrset: {
      				name: 'Attr 3',
      				id: 3
  				},
			    status: 'Active',
			    action: {
			    	id: 3
			    }
			},
			{
	    		id: 4,
			    attrset: {
      				name: 'Attr 4',
      				id: 4
  				},
			    status: 'Active',
			    action: {
			    	id: 4
			    }
			}
		]

		const columns = [{
		    Header: 'Id',
		    accessor: 'id',
		    show: false
		},
		{
		    Header: 'Attribute Set',
		    accessor: 'attrset',
			Cell: row => {
              	return (
                	<div>
                  		<Link to={'attrset/add/'+row.row.attrset.id}>{row.row.attrset.name}</Link>
            		</div>
              	)
            }
		},
		{
		    Header: 'Status',
		    accessor: 'status'
		},
		{
		    Header: 'Action',
		    accessor: 'action',
		    Cell: row => {
		    	return (
                	<div>
                		<Link to={'attribute-option/'+row.row.action.id}>{'Add'}</Link>
                	</div>
              	)
            }
		}]

        return (
		    <React.Fragment>
		      	<Breadcrumb>
					<BreadcrumbItem active>Attribute Set</BreadcrumbItem>
					<div class="addTopFrmBtnWrapper"><Link to={'/attribute-set/add'} class="addTopFrmBtn">Add</Link></div>
				</Breadcrumb>
		      	<Row>
		      		<Col sm="12">
						<ReactTable
							data={data}
							columns={columns}

							defaultPageSize={5}
							className="-striped -highlight"
						/>
		      		</Col>
		      	</Row>
			</React.Fragment >
        )
    }
}
export default AttributeSetList;