import React, { Component } from 'react';
import { Form, Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './Login.css';
import { loginUser } from '../../../actions/user-actions';
class Login extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
       		fields: {},
           	errors: {}
		}
	}
	
	handleValidation(){
    	let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        
        if(!fields["username"]){
       		formIsValid = false;
           	errors["username"] = "Cannot be empty";
        }

        if(typeof fields["username"] !== "undefined"){
           	if(!fields["username"].match(/^[a-zA-Z]+$/)){
          		formIsValid = false;
              	errors["username"] = "Only letters";
           	}
        }

        if(!fields["password"]){
       		formIsValid = false;
        	errors["password"] = "Cannot be empty";
        }

       	this.setState({errors: errors});
       	return formIsValid;
   	}

  	handleSubmit(event) {
    	event.preventDefault();
		
        if(this.handleValidation()){
			   //alert('A name was submitted: ' + this.state.value);
			const user = { "username": this.state.fields["username"], "password": this.state.fields["password"]};
			//this.props.loginUser(user);
			console.log(user);
        }else{
           	//alert("Form has errors.")
        }
  	}

  	handleChange(field, e){         
    	let fields = this.state.fields;
		fields[field] = e.target.value;
			
			this.state.fields = fields;
    }

  render() {
    return (
		<div className="app flex-row align-items-center">
			<Container>
				<Row className="justify-content-center">
					<Col md="8">
						<CardGroup>
							<Card className="p-4">
								<CardBody>
									<Form onSubmit={this.handleSubmit.bind(this)}>
										<h1>Login</h1>
										<p className="text-muted">Sign In to your account</p>
										<InputGroup className="mb-3">
											<InputGroupAddon addonType="prepend">
												<InputGroupText>
													<i className="fa fa-user"></i>
												</InputGroupText>
											</InputGroupAddon>
											<Input ref="username" type="text" placeholder="Username" onChange={this.handleChange.bind(this, "username")} value={this.state.fields["username"]} />
											<span style={{color: "red", width: "100%"}}>{this.state.errors["username"]}</span>
										</InputGroup>
										<InputGroup className="mb-4">
											<InputGroupAddon addonType="prepend">
												<InputGroupText>
													<i className="fa fa-lock"></i>
												</InputGroupText>
											</InputGroupAddon>
											<Input ref="password" type="password" placeholder="Password" onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} />
											<span style={{color: "red", width: "100%"}}>{this.state.errors["password"]}</span>
										</InputGroup>
										<Row>
											<Col xs="6">
												<Button color="danger" className="px-4" type="submit">Login</Button>
											</Col>
											<Col xs="6" className="text-right">
												<Link to = "/forgetpassword" className="px-0 btn btn-link"> Forgot password? </Link>
											</Col>
										</Row>
									</Form>
								</CardBody>
							</Card>
							<Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
								<CardBody className="text-center">
									<div>

									</div>
								</CardBody>
							</Card>
						</CardGroup>
					</Col>
				</Row>
			</Container>
		</div>
    );
  }
}


function mapStateToProps(state) {
	return {
		user: state.userStore.user,
		errors: state.userStore.errors
	}
}
export default connect(mapStateToProps,{loginUser})(Login);
