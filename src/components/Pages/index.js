import Login from './Login';
import ForgetPassword from './ForgetPassword';
import Page404 from './Page404';
import Page500 from './Page500';

export {
  Login, ForgetPassword, Page404, Page500
};