import React, { Component } from 'react';
import { Form, Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import './ForgetPassword.css';

class ForgetPassword extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
       		fields: {},
           	errors: {}
		}
	}
	
	handleValidation(){
		let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        
        if(!fields["emailid"]){
           	formIsValid = false;
           	errors["emailid"] = "Cannot be empty";
        }
        if(typeof fields["emailid"] !== "undefined"){
           	let lastAtPos = fields["emailid"].lastIndexOf('@');
           	let lastDotPos = fields["emailid"].lastIndexOf('.');

           	if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["emailid"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["emailid"].length - lastDotPos) > 2)) {
              	formIsValid = false;
              	errors["emailid"] = "Email is not valid";
        	}
       	}

   		this.setState({errors: errors});
       	return formIsValid;
   	}

  	handleSubmit(event) {
    	event.preventDefault();

        if(this.handleValidation()){
       		alert('A name was submitted: ' + this.state.value);
        }else{
       		//alert("Form has errors.")
    	}
  	}

  	handleChange(field, e){         
    	let fields = this.state.fields;
        fields[field] = e.target.value;        
        this.setState({fields});
    }

  render() {
    return (
		<div className="app flex-row align-items-center">
			<Container>
				<Row className="justify-content-center">
					<Col md="8">
						<CardGroup>
							<Card className="p-3">
								<CardBody>
									<Form onSubmit={this.handleSubmit.bind(this)}>
										<h1>Forget Password</h1>
										<p className="text-muted">Enter your email id</p>
										<InputGroup className="mb-3">
											<InputGroupAddon addonType="prepend">
												<InputGroupText>
													<i className="fa fa-user"></i>
												</InputGroupText>
											</InputGroupAddon>
											<Input ref="emailid" type="text" placeholder="Email Id" onChange={this.handleChange.bind(this, "emailid")} value={this.state.fields["emailid"]} />
											<span style={{color: "red", width: "100%"}}>{this.state.errors["emailid"]}</span>
										</InputGroup>
										<Row>
											<Col xs="6">
												<Button color="danger" className="px-4" type="submit">Retrieve Password</Button>
											</Col>
											<Col xs="6" className="text-right">
												<Link to = "/login" className="px-0 btn btn-link"> Login? </Link>
											</Col>
										</Row>
									</Form>
								</CardBody>
							</Card>
							<Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
								<CardBody className="text-center">
									<div>

									</div>
								</CardBody>
							</Card>
						</CardGroup>
					</Col>
				</Row>
			</Container>
		</div>
    );
  }
}

export default ForgetPassword;
