import React from 'react';
import Loadable from 'react-loadable'
import DefaultLayout from './components/Layouts/DefaultLayout';
import LteLayout from './components/Layouts/LteLayout';

function Loading() {
    return <div> Loading... </ div>;
}

const Dashboard = Loadable({
    loader: () =>
        import ('./components/Views/Dashboard'),
    loading: Loading,
});

const Profile = Loadable({
    loader: () =>
        import ('./components/Views/Profile'),
    loading: Loading,
});

const ListCategory = Loadable({
    loader: () =>
        import ('./components/Views/Category/ListCategory'),
    loading: Loading,
});

const AddCategory = Loadable({
    loader: () =>
        import ('./components/Views/Category/AddCategory'),
    loading: Loading,
});

const AttributeSetList = Loadable({
    loader: () =>
        import ('./components/Views/AttributeSet/AttributeSetList'),
    loading: Loading,
});

const AttributeSetAdd = Loadable({
    loader: () =>
        import ('./components/Views/AttributeSet/AttributeSetAdd'),
    loading: Loading,
});

const AttributeOptionAdd = Loadable({
    loader: () =>
        import ('./components/Views/AttributeOption/AttributeOptionAdd'),
    loading: Loading,
});

const ProductList = Loadable({
    loader: () =>
        import ('./components/Views/Product/ProductList'),
    loading: Loading,
});

const ProductAdd = Loadable({
    loader: () =>
        import ('./components/Views/Product/ProductAdd'),
    loading: Loading,
});

const ChangePassword = Loadable({
    loader: () =>
        import ('./components/Views/ChangePassword'),
    loading: Loading,
});


const routes = [
    {
        path: '/',
        exact: true,
        name: 'Home',
        component: LteLayout
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile
    },
       
    {
        path: '/category/add',
        name: 'AddCategory',
        component: AddCategory
    } ,
    {
        path: '/category/edit/:_id',
        name: 'AddCategory',
        component: AddCategory
    } ,
    {
        path: '/category',
        name: 'ListCategory',
        component: ListCategory
    },    
    {
        path: '/attribute-set/add',
        name: 'AttributeSetAdd',
        component: AttributeSetAdd
    },
    {
        path: '/attribute-set',
        name: 'AttributeSetList',
        component: AttributeSetList
    },
    {
        path: '/attribute-option',
        name: 'AttributeOptionAdd',
        component: AttributeOptionAdd
    },    
    {
        path: '/product/add',
        name: 'ProductAdd',
        component: ProductAdd
    },
    {
        path: '/product',
        name: 'ProductList',
        component: ProductList
    },
    {
        path: '/change-password',
        name: 'ChangePassword',
        component: ChangePassword
    }       
];

export default routes;
