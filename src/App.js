import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';

import {  DefaultLayout, LteLayout } from './components/Layouts';
import { Login, ForgetPassword } from './components/Pages';

class App extends Component {
  render() {
    return (
		<BrowserRouter>
	        <Switch>
				<Route path="/login" name="Home" component={Login} />
				<Route path="/forgetpassword" name="Home" component={ForgetPassword} />
				<Route path="/" name="Home" component={LteLayout} />
		    </Switch>
		</BrowserRouter>
      
    );
  }
}

export default App;
