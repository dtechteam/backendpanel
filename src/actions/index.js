import axios from "axios";

export const client = axios.create({
  baseURL: "https://order-system-api.herokuapp.com/api",
  headers: {
    "Content-Type": "application/json"
  }
})
