import { client } from './';

const url = '/login';


export function loginUser(user) {
    return dispatch => {
        return dispatch({
            type: 'LOGIN',
            payload: client.post(url, user)
        })
    }
}
