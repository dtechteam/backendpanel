import { client } from './';

const url = '/category';

export function fetchCategories(){
  return dispatch => {
    dispatch({
      type: 'FETCH_CATEGORIES',
      payload: client.get(url)
    })
  }
}

export function newCategory() {
  return dispatch => {
    dispatch({
      type: 'NEW_CATEGORY'
    })
  }
}

export function saveCategory(category) {
  return dispatch => {
    return dispatch({
      type: 'SAVE_CATEGORY',
      payload: client.post(url, category)
    })
  }
}

export function fetchCategory(_id) {
  return dispatch => {
    return dispatch({
      type: 'FETCH_CATEGORY',
      payload: client.get(`${url}/${_id}`)
    })
  }
}

export function updateCategory(category) {
  return dispatch => {
    return dispatch({
      type: 'UPDATE_CATEGORY',
      payload: client.put(`${url}/${category._id}`, category)
    })
  }
}

export function deleteCategory(_id) {
  return dispatch => {
    return dispatch({
      type: 'DELETE_CATEGORY',
      payload: client.delete(`${url}/${_id}`)
    })
  }
}
