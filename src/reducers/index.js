import { combineReducers } from 'redux';
import CategoryReducer from './category-reducer';
import UserReducer from './user-reducer';
import { reducer as formReducer } from 'redux-form';

const reducers = {
  categoryStore: CategoryReducer,
  userStore: UserReducer,
  form: formReducer
}

const rootReducer = combineReducers(reducers);

export default rootReducer;
