const defaultState = {
  categories: [],
  category: {name:{}},
  loading: false,
  errors:{}
}

export default (state=defaultState, action={}) => {
  switch (action.type) {
    case 'FETCH_CATEGORIES_FULFILLED': {
      return {
        ...state,
        categories: action.payload.data,
        loading: false,
        errors: {}
      }
     
    }

    case 'FETCH_CATEGORIES_PENDING': {
      return {
        ...state,
        loading: true,
        errors: {}
      }
    }

    case 'FETCH_CATEGORIES_REJECTED': {
      return {
        ...state,
        loading: false,
        errors: { global: action.payload.message }
      }
    }

    case 'NEW_CATEGORY': {
      return {
        ...state,
        category: {name:{}}
      }
    }

    case 'SAVE_CATEGORY_PENDING': {
      return {
        ...state,
        loading: true
      }
    }

    case 'SAVE_CATEGORY_FULFILLED': {
      return {
        ...state,
        categories: [...state.categories, action.payload.data],
        errors: {},
        loading: false
      }
    }

    case 'SAVE_CATEGORY_REJECTED': {
      const data = action.payload.response.data;
      // convert feathers error formatting to match client-side error formatting
      const { "name.first":first, "name.last":last, phone, email } = data.errors;
      const errors = { global: data.message, name: { first,last }, phone, email };
      return {
        ...state,
        errors: errors,
        loading: false
      }
    }

    case 'FETCH_CATEGORY_PENDING': {
      return {
        ...state,
        loading: true,
        category: {name:{}}
      }
    }

    case 'FETCH_CATEGORY_FULFILLED': {
      return {
        ...state,
        category: action.payload.data,
        errors: {},
        loading: false
      }
    }

    case 'UPDATE_CATEGORY_PENDING': {
      return {
        ...state,
        loading: true
      }
    }

    case 'UPDATE_CATEGORY_FULFILLED': {
      const category = action.payload.data;
      return {
        ...state,
        categories: state.categories.map(item => item._id === category._id ? category : item),
        errors: {},
        loading: false
      }
    }

    case 'UPDATE_CATEGORY_REJECTED': {
      const data = action.payload.response.data;
      const { name } = data.errors;
      const errors = { global: data.message, name };
      return {
        ...state,
        errors: errors,
        loading: false
      }
    }

    case 'DELETE_CATEGORY_FULFILLED': {
      const _id = action.payload.data._id;
      return {
        ...state,
        categories: state.categories.filter(item => item._id !== _id)
      }
    }

    default:
      return state;
  }
}
