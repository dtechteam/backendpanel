const defaultState = {
  categories: [],
  category: {name:{}},
  loading: false,
  errors:{}
}

export default (state=defaultState, action={}) => {
  switch (action.type) {
   
    case 'LOGIN_PENDING': {
      return {
        ...state,
        loading: true
      }
    }

      case 'LOGIN_FULFILLED': {
      return {
        ...state,
        user: [...state.user, action.payload.data],
        errors: {},
        loading: false
      }
    }

      case 'LOGIN_REJECTED': {
      const data = action.payload.response.data;
      // convert feathers error formatting to match client-side error formatting
          const { username, password } = data.errors;
          const errors = { global: data.message, username, password };
      return {
        ...state,
        errors: errors,
        loading: false
      }
    }

    default:
      return state;
  }
}
